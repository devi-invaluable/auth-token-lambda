package com.invaluable.token;

import java.util.HashMap;

/**
 * Created by dchandrasekaran on 4/26/18.
 */


public class TokenLambdaRequest {
    HashMap<String, String> queryStringParameters;

    public HashMap<String, String> getQueryStringParameters() {
        return queryStringParameters;
    }

    public void setQueryStringParameters(HashMap<String, String> queryStringParameters) {
        this.queryStringParameters = queryStringParameters;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    String username;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    String password;

}
