package com.invaluable.token;

/**
 * Created by dchandrasekaran on 4/26/18.
 */
public class TokenLambdaResponse {
    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    String statusCode;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    String token;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    String error;


}
