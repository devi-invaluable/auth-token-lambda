package com.invaluable.token;

import com.amazonaws.services.cognitoidp.AWSCognitoIdentityProviderClientBuilder;
import com.amazonaws.services.cognitoidp.model.AdminInitiateAuthRequest;
import com.amazonaws.services.cognitoidp.model.AdminInitiateAuthResult;
import com.amazonaws.services.cognitoidp.model.AuthFlowType;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by dchandrasekaran on 4/26/18.
 */
public class TokenGeneratorRequestHandler implements RequestHandler<TokenLambdaRequest, TokenLambdaResponse> {



    public TokenLambdaResponse handleRequest(TokenLambdaRequest request, Context context) {


        String userName = request.getUsername();
        String pwd = request.getPassword();

        String token = null;
        String error = null;
        try {
            token = getUserToken(userName, pwd);
        } catch (Exception e) {
            e.printStackTrace();
        }

        context.getLogger().log("token: " + token);
        System.out.println("User token:\n" + token);
        if (token != null) {
            System.out.println("\nUser token attributes:\n"
                    + token);
        } else {
            error = "Username/password is invalid";
        }

        TokenLambdaResponse tokenLambdaResponse = new TokenLambdaResponse();
        tokenLambdaResponse.setToken(token);


        if (error !=null) { tokenLambdaResponse.setError(error);}
        return tokenLambdaResponse;

    }


    String getUserToken(String userName, String pwd) throws Exception {

        Map<String,String> authParams = new HashMap<String, String>();

        authParams.put("USERNAME", userName);
        authParams.put("PASSWORD", pwd);

        System.out.println(authParams.get("USERNAME") + " hello");
        //System.getenv("NAME_OF_YOUR_ENV_VARIABLE")

        String clientID = System.getenv("clientID");
        String userPoolID = System.getenv("userPoolID");

        AdminInitiateAuthResult result =
                AWSCognitoIdentityProviderClientBuilder
                        .standard()
                        .defaultClient()
                        .adminInitiateAuth(new AdminInitiateAuthRequest()
                                .withAuthFlow(AuthFlowType.ADMIN_NO_SRP_AUTH)
                                .withAuthParameters(authParams)
                                .withClientId(clientID)
                                .withUserPoolId(userPoolID));
                               // .withClientId("6h9igp2s1aai5ol5qtkhanugdq")
                               // .withUserPoolId("us-east-1_U1E02WGwg"));


        System.out.println("ENV Variables: ClientID =  "+ System.getenv("clientID"));

        if (result == null) {
            throw new Exception("Invalid Credentials");
        }
        return result.getAuthenticationResult().getIdToken();
    }


}





